#!/usr/bin/env python
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf

from sklearn.neighbors import KNeighborsClassifier as KNN
from sklearn.datasets import *
from sklearn import preprocessing
from tensorflow.keras import layers, models

import neurolab as nl
from sklearn.preprocessing import OneHotEncoder



SZ=20
bin_n = 16 # Number of bins
affine_flags = cv.WARP_INVERSE_MAP|cv.INTER_LINEAR
mycol = 28
myrow = 6
def deskew(img):
    m = cv.moments(img)
    if abs(m['mu02']) < 1e-2:
        return img.copy()
    skew = m['mu11']/m['mu02']
    M = np.float32([[1, skew, -0.5*SZ*skew], [0, 1, 0]])
    img = cv.warpAffine(img,M,(SZ, SZ),flags=affine_flags)
    return img
def hog(img):
    gx = cv.Sobel(img, cv.CV_32F, 1, 0)
    gy = cv.Sobel(img, cv.CV_32F, 0, 1)
    mag, ang = cv.cartToPolar(gx, gy)
    bins = np.int32(bin_n*ang/(2*np.pi))    # quantizing binvalues in (0...16)
    bin_cells = bins[:10,:10], bins[10:,:10], bins[:10,10:], bins[10:,10:]
    mag_cells = mag[:10,:10], mag[10:,:10], mag[:10,10:], mag[10:,10:]
    hists = [np.bincount(b.ravel(), m.ravel(), bin_n) for b, m in zip(bin_cells, mag_cells)]
    hist = np.hstack(hists)     # hist is a 64 bit vector
    return hist
img = cv.imread(cv.samples.findFile('digits.png'),0)
print(img.shape)
if img is None:
    raise Exception("we need the digits.png image from samples/data here !")
cells = [np.hsplit(row,mycol) for row in np.vsplit(img,myrow)]
#plt.imshow(cells[4][6])
#plt.show()
# First half is trainData, remaining is testData
train_cells = [ i[:mycol//2] for i in cells]
test_cells = [ i[mycol//2:] for i in cells]
deskewed = [list(map(deskew,row)) for row in train_cells]
hogdata = [list(map(hog,row)) for row in deskewed]
trainData = np.float32(hogdata).reshape(-1,64)
responses = np.repeat(np.arange(myrow),mycol//2)[:,np.newaxis]
#Support vector machine - SVM
svm = cv.ml.SVM_create()
svm.setKernel(cv.ml.SVM_LINEAR)
svm.setType(cv.ml.SVM_C_SVC)
svm.setC(2.67)
svm.setGamma(5.383)
#print(trainData.shape, responses.shape)
svm.train(trainData, cv.ml.ROW_SAMPLE, responses)
svm.save('svm_data.dat')
deskewed = [list(map(deskew,row)) for row in test_cells]
hogdata = [list(map(hog,row)) for row in deskewed]
testData = np.float32(hogdata).reshape(-1,bin_n*4)
result = svm.predict(testData)[1]
mask = result==responses
correct = np.count_nonzero(mask)
print("SVM accuracy: ", correct*100.0/result.size)

#KNN
#ns = np.arange(1,80)
#for i in ns: Used this to test n_neighbor gave the best results. 
#1 and 19 gave the same result and was higher than all other numbers between 1 and 70
neigh = KNN(n_neighbors = 19)
responsesKNN = responses.reshape(-1)
neigh.fit(trainData, responsesKNN)
resultKNN = neigh. predict(testData)
resultKNN = resultKNN.reshape(resultKNN.shape[0],1)
mask = resultKNN == responses
correct = np.count_nonzero(mask)
print("KNN accuracy: ", correct*100.0/len(resultKNN))


knn = cv.ml.KNearest_create()
knn.train(trainData, cv.ml.ROW_SAMPLE, responses)
ret, result, neighbours, dist = knn.findNearest(testData, k=19)

matches = result==responses
correct = np.count_nonzero(matches)
accuracy = correct*100.0/result.size

print(accuracy)

#Tensorflow
train_data = np.array(train_cells)
test_data = np.array(test_cells)
train_data, test_data = train_data / 255, test_data / 255
train_data = train_data.reshape(train_data.shape[0]*train_data.shape[1], train_data.shape[2], train_data.shape[3],1)
imshape = train_data.shape
test_data = test_data.reshape(imshape)
print(train_data.shape, test_data.shape)

letter =  ["a", "b", "c", "d", "e", "f", "g"]

#plt.figure(figsize=(10,10))
#for i in range(25):
    #plt.subplot(5, 5, i + 1)
    #plt.xticks([])
    #plt.yticks([])
    #plt.grid(False)
    #plt.imshow(train_data[i], cmap= 'gray', vmin = 0, vmax = 1)
    #plt.xlabel(letter[responses[i][0]])
#plt.show()

model = models.Sequential()
model.add(layers.Conv2D(30, (3, 3), activation="relu", input_shape=imshape[1:]))
model.add(layers.MaxPooling2D((2,2)))
model.add(layers.Conv2D(64, (3,3), activation="relu"))
model.add(layers.MaxPooling2D((2,2)))
model.add(layers.Conv2D(64, (3,3), activation ="relu"))

model.add(layers.Flatten())
model.add(layers.Dense(500, activation="relu"))
model.add(layers.Dense(300, activation="relu"))
model.add(layers.Dense(100, activation="relu"))
model.add(layers.Dense(50, activation="relu"))
model.add(layers.Dense(len(letter)))

model.compile(optimizer="adam", loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),metrics=["accuracy"])
history = model.fit(train_data, responses, epochs = 100, validation_data=(test_data, responses))

plt.plot(history.history["accuracy"], label="accuracy")
plt.plot(history.history["val_accuracy"], label="val_accuracy")
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
plt.ylim([0,1])
plt.legend(loc="lower right")
plt.show()
test_loss, test_acc = model.evaluate(test_data, responses, verbose=2)
