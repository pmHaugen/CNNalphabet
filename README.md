# Recognice handwriting with TensorFlow CNN
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Uses convolutional neural network to teach the program to recognice handwritten letters

![alt picture](stats.png)
## Requirments
python 3.10.4

Dependencies
```
pip install numpy
pip install sklearn
pip install opencv_python
pip install matplotlib
pip install tensorflow

```
## Usage
```
python3 main.py
```

### Contributing
*Paal Marius Haugen

## License
[MIT](docs/LICENSE.md)
